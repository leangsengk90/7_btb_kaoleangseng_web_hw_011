import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Card, Button, Form } from "react-bootstrap";
import History from "./History";
import "../App.css";

export default class Calculator extends Component {
  constructor() {
    super();
    this.state = {
      value1: "",
      value2: "",
      total: 0,
      operator: "+",
      resultList: [],
      item: null,
      sms: "",
      smsEmpty: "Please input in the field!",
      smsOnlyNum: "Please input only number!",
      term1: null,
      term2: null,
    };
  }

  clearTable = () => {
    this.setState({
      resultList: [],
      item: null,
    })
  }

  handleChange(event) {
    this.setState(
      {
        //[event.target.name]:event.target.value,
        value1: event.target.value,
      },
      () => {
        const reg = /[^0-9]/gi;
        this.setState({
          term1: reg.test(this.state.value1),
        });
      }
    );
  }

  handleChange2(event) {
    this.setState(
      {
        //[event.target.name]:event.target.value,
        value2: event.target.value,
      },
      () => {
        const reg = /[^0-9]/gi;
        this.setState({
         // term1: reg.test(this.state.value1),
          term2: reg.test(this.state.value2),
        });
      }
    );
  }

  calculate = () => {
    console.log("TERM1:", this.state.term1);
    console.log("TERM2:", this.state.term2);
    this.setState(
      () => {
        if (
          this.state.value1 !== "" &&
          this.state.value2 !== "" &&
          this.state.term1 === false &&
          this.state.term2 === false
        ) {
          if (this.state.operator === "+") {
            return {
              total: parseInt(this.state.value1) + parseInt(this.state.value2),
            };
          } else if (this.state.operator === "-") {
            return {
              total: parseInt(this.state.value1) - parseInt(this.state.value2),
            };
          } else if (this.state.operator === "*") {
            return {
              total: parseInt(this.state.value1) * parseInt(this.state.value2),
            };
          } else if (this.state.operator === "/") {
            return {
              total: parseInt(this.state.value1) / parseInt(this.state.value2),
            };
          } else if (this.state.operator === "%") {
            return {
              total: parseInt(this.state.value1) % parseInt(this.state.value2),
            };
          }
        }
      },
      () => {
        if (
          this.state.value1 !== "" &&
          this.state.value2 !== "" &&
          this.state.term1 === false &&
          this.state.term2 === false
        ) {
          this.state.resultList.push(this.state.total);
          this.setState({
            sms: "",
            value1: "",
            value2: "",
            item: this.state.resultList.map((e, index) => (
              <tr key={index}>
                <td>{String(e)}</td>
              </tr>
            )),
          });
        } else {
          this.setState({
            sms: this.state.smsEmpty,
          });
        }
      }
    );
  };

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-6">
            <Card>
              <Card.Img
                variant="top"
                src="https://cdn.dribbble.com/users/941071/screenshots/2340813/day05calculatoricon.png"
              />
              <Card.Body>
                <Card.Title>Calculator</Card.Title>
                <Form>
                  <Form.Group controlId="formBasicEmail">
                    {/* <Form.Label>Email address</Form.Label> */}
                    <Form.Control
                      type="text"
                      placeholder="Value 1"
                      name="value1"
                      value={this.state.value1}
                      onChange={this.handleChange.bind(this)}
                    />
                    <Form.Text className="alert-sms">
                      &nbsp;
                      {this.state.value1 === ""
                        ? this.state.sms
                        : this.state.term1 === true
                        ? this.state.smsOnlyNum
                        : ""}
                    </Form.Text>
                  </Form.Group>

                  <Form.Group controlId="formBasicEmail">
                    {/* <Form.Label>Email address</Form.Label> */}
                    <Form.Control
                      type="text"
                      placeholder="Value 2"
                      name="value2"
                      value={this.state.value2}
                      onChange={this.handleChange2.bind(this)}
                    />
                    <Form.Text className="alert-sms">
                      &nbsp;
                      {this.state.value2 === ""
                        ? this.state.sms
                        : this.state.term2 === true
                        ? this.state.smsOnlyNum
                        : ""}
                    </Form.Text>
                  </Form.Group>
                  <Form.Group controlId="exampleForm.ControlSelect1">
                    {/* <Form.Label>Example select</Form.Label> */}
                    <Form.Control
                      as="select"
                      onChange={(e) =>
                        this.setState({ operator: e.target.value })
                      }
                    >
                      <option value="+">+ Plus</option>
                      <option value="-">- Subtract</option>
                      <option value="*">* Multiply</option>
                      <option value="/">/ Divide</option>
                      <option value="%">% Module</option>
                    </Form.Control>
                  </Form.Group>
                  <Button
                    id="btn-cal"
                    variant="primary"
                    type="button"
                    onClick={this.calculate}
                  >
                    &#10148; Calculate
                  </Button>
                </Form>
              </Card.Body>
            </Card>
          </div>
          <div className="col-md-6">
            <History getValue={this.state.item} clear={this.clearTable} />
          </div>
        </div>
      </div>
    );
  }
}
