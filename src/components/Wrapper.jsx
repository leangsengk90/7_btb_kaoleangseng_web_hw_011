import React, { Component } from "react";
import Calculator from "./Calculator";
import '../App.css';

export default class Wrapper extends Component {
  render() {
    return (
      <div className="wrapper">
        <Calculator />
      </div>
    );
  }
}
